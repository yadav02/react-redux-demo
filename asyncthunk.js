const redux = require ('redux');
const axios = require ('axios');
const thunkMiddleware = require('redux-thunk').default
const applyMiddleware = redux.applyMiddleware;
const createStore = redux.createStore;

const initialState = {
  loading:false,
  user:[],
  error:''
}

const userRequest = () => {
  return{
    type:'User_Request'

  }
}
const userSuccess = (users) => {
  return{
    type:'User_Success',
    payload:users
  }
}
const userError = (error) => {
  return{
    type:'User_Error',
    payload: error
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'User_Request':return{
      ...state,
      loading:true
    }
    case 'User_Success':return{
      loading:false,
      user:action.payload,
      error:''
    }
    case 'User_Error':return{
      loading:false,
      users:[],
      error:action.payload
    }
  }
}

const fetchUser = () =>{
  return (dispatch) => {
    dispatch(userRequest());
    axios.get('https://jsonplaceholder.typicode.com/todos')
    .then(response => {
      const users = response.data.map(todo => todo.title)
      dispatch(userSuccess(users))
    })
    .catch(error =>{
      dispatch(userError(error.message))
    })
  }
}

const store = createStore(reducer, applyMiddleware(thunkMiddleware));
store.subscribe(()=> console.log(store.getState()));
store.dispatch(fetchUser());
