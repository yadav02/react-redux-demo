const redux = require('redux');
const createStore = redux.createStore;

const initialState ={
  numberOfBooks: 100
}

function buyBook (){
  return{
  type:"BUY_BOOK",
  info:"My First React-Redux EXMP"
  }
}

const reducer = (state=initialState, action) => {

switch (action.type) {
  case "BUY_BOOK":return{
    numberOfBooks:state.numberOfBooks - 1
  }
  default: return state
    }
  }

  const store = createStore(reducer);
  console.log("Initial state vlaue", store.getState());

  const unsubscribe = store.subscribe(()=>console.log('Updated state Value', store.getState()));
  store.dispatch(buyBook());
  store.dispatch(buyBook());
  unsubscribe();
